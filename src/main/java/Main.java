import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static final String FILEPATH = "src/main/resources/grammar.txt";

    public static void main(String[] args) {
        File grammarFile = new File(FILEPATH);
        FileParser fileParser = new FileParser();
        Grammar grammar = fileParser.parseFile(grammarFile);
        String terminals[] = {"a", "b"};
        if (grammar.isNotEmpty(new ArrayList<String>(Arrays.asList(terminals)), "S")) {
            System.out.println("GOOD! Grammar is not empty!");
        } else System.out.println("Grammar is empty!");
    }
}
