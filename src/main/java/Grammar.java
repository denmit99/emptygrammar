import java.util.ArrayList;

public class Grammar {
    private ArrayList<Rule> rules = new ArrayList<Rule>();

    public void addRule(Rule r) {
        rules.add(r);
    }

    public boolean isNotEmpty(ArrayList<String> nonTerminals, String startTerminal) {

        /*
        * Проверка грамматики на пустоту
        * На вход подаются нетерминальные символы  и стартовый нетерминал
        * Возвращет true, если грамматика непустая и наоборот
        * */

        ArrayList<String> previousTerminals;
        ArrayList<String> terminals = new ArrayList<String>();

        while (true) {
            previousTerminals = new ArrayList<String>(terminals);
            ArrayList<String> newTerminals = getNewTerminals(nonTerminals, terminals);
            terminals.addAll(newTerminals);
            System.out.println(terminals);
            if (compareTerminalSet(previousTerminals, terminals)) {
                if (terminals.contains(startTerminal)) {
                    return true;
                } else return false;
            }
        }
    }

    public boolean isRightChain(String chain, ArrayList<String> nonTerminals, ArrayList<String> terminals) {

        /*
        * Возвращает true, если все символы в правой части правила принадлежат множеству нетерминалов или
        * текущему множеству терминалов
        * */

        for (char c : chain.toCharArray()) {
            if (!isTerminal(c, terminals) && !isNonTerminal(c, nonTerminals)) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<String> getNewTerminals(ArrayList<String> nonTerminals, ArrayList<String> terminals) {

        /*
        * Возвращает новые терминалы для добавления в множество терминалов
        * */

        ArrayList<String> newTerminals = new ArrayList<String>();
        for (Rule r : rules) {
            if (isRightChain(r.getRightPart(), nonTerminals, terminals)&&!terminals.contains(r.getLeftPart())) {
                newTerminals.add(r.getLeftPart());
            }
        }
        return newTerminals;
    }

    public boolean isTerminal(char s, ArrayList<String> terminals) {

        /*
         * Возвращает true, если символ s терминальный
         * */

        for (String t : terminals) {
            if (t.equals(String.valueOf(s))) {
                return true;
            }
        }
        return false;
    }

    public boolean isNonTerminal(char s, ArrayList<String> nonTerminals) {

        /*
         * Возвращает true, если символ s нетерминальный
         * */

        for (String n : nonTerminals) {
            if (n.equals(String.valueOf(s))) {
                return true;
            }
        }
        return false;
    }

    public boolean compareTerminalSet(ArrayList<String> t1, ArrayList<String> t2) {

        /*
        * Возвращает true если два множества равны
        * */

        if (t1.size() != t2.size()) {
            return false;
        }
        for (String s : t1) {
            if (!t2.contains(s)) {
                return false;
            }
        }
        return true;
    }
}